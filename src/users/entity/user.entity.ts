import {
    Entity,
    PrimaryGeneratedColumn, 
    Column,
    CreateDateColumn,
    UpdateDateColumn
} from 'typeorm';
import {Length, IsNotEmpty,IsEmail} from 'class-validator';
import * as bcrypt from 'bcryptjs';

@Entity()
export class UserEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column ({default:null})
    @Length(4,50)
    name: string;

    @IsEmail(undefined, { message: "Username is not a valid email address." })
    @Column({default:null})
    @Length(3,50)
    email : string;

    @Column({default:null})
    password : string;

    @Column({default:'user'})
    @IsNotEmpty()
    role: string;
    
    @Column({default:null})
    @CreateDateColumn()
    createdAt : Date;

    @Column({default:null})
    @UpdateDateColumn()
    updateAt: Date;

    hashPassword(){
        this.password = bcrypt.hashSync(this.password, 8);
    }

    checkIfUnencryptedPasswordIsValid(unencryptedPassword: string) {
        return bcrypt.compareSync(unencryptedPassword, this.password);
    }
}