import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import {UsersService} from '../users/users.service';
import { UserEntity } from 'src/users/entity/user.entity';
import e = require('express');
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs'; 


@Injectable()
export class AuthService {
    jwt: any;
    constructor(
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService){}

    async validateUser(email: string, password: string) : Promise<any>{
        const user = await this.usersService.findbyemail(email);
        if(user && user.password === password){
            const{password, ...result} = user;
            return result;
        }
        return null;
    }

    login(user:UserEntity){
        const payload = {email: user.email, id: user.id};
        return{
            access_token : this.jwtService.sign(payload),
        };
    }

    async validateLogin(email,password){
        var userdb = await this.usersService.findbyemail(email);
        if(!userdb)  throw new HttpException('LOGIN.USER_NOT_FOUND', HttpStatus.NOT_FOUND);
        
        var isValidPass = await bcrypt.compare(password, userdb.password);
        if(isValidPass){
            var accesstoken =  this.login(userdb);
            return accesstoken;
        }
        else{
            throw new HttpException('LOGIN.ERROR', HttpStatus.UNAUTHORIZED);
        }
    }
}
