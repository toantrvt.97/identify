import { Controller, Get, Param, Post, Body, Put, Delete } from '@nestjs/common';
import {UsersService} from './users.service';
import {UserEntity} from './entity/user.entity';
import { AuthGuard } from '@nestjs/passport';

@Controller('users')
export class UsersController {
    constructor (private readonly usersService: UsersService ){}
    @Get()
    findAll(): Promise<UserEntity[]>{
        return this.usersService.findAll();
    }
    @Get(':email')
    async get(@Param() params){
        try {
            var user =  await this.usersService.findbyemail(params.email);
            return  user;
          } catch(error){
            return  error;
          }
    }
    @Post('create')
    create(@Body() user:UserEntity){
        return this.usersService.create(user);
    }
    @Put('update')
    update(@Body() user:UserEntity) {
        return this.usersService.update(user);
    }
    @Delete(':id')
    delete(@Param() params) {
        return this.usersService.delete(params.id);
    }
}
