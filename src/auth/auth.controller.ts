import { Controller, Request, Post, UseGuards, Get, HttpCode, HttpStatus, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {Login} from './interfaces/login.interface';
import {AuthService} from './auth.service';
import {UsersService} from '../users/users.service';
import { UserEntity } from 'src/users/entity/user.entity';
@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService : UsersService
  ){}
    // @UseGuards(AuthGuard('local'))
    // @Post('auth/login')
    // async login(@Request() req) {
    //     return req.user;
    // }
    // @UseGuards(AuthGuard('Admin'))
    // @Get('profile')
    // getProfile(@Request() req) {
    //   return req.user;
    // }
    @Post('login')
    public async login(@Body() login: Login){
      try {
        var respone = await this.authService.validateLogin(login.email,login.password);
        return respone;

      }
      catch(e){
        return e;
      }
    }
    @Post('register')
    async register(@Body() user: UserEntity){
      try {
        var newUser = await this.userService.createNewUser(user);
        return newUser;
      }
      catch(e){
        return e;
      }
    }
}
