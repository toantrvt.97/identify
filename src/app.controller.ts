import { Controller, Request, Post, UseGuards, Get, Body } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthGuard } from '@nestjs/passport';
import { UsersService } from './users/users.service';
import { Login } from './auth/interfaces/login.interface';
import {AuthService} from './auth/auth.service';
import { UserEntity } from 'src/users/entity/user.entity';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService,
    private readonly userService: UsersService,
    private readonly authService: AuthService,) { }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
  // // @UseGuards(AuthGuard('local'))
  //   @Post('auth/login')
  //   async login(@Request() req) {
  //       return req.user;
  //   }
  // //  @UseGuards(AuthGuard('jwt'))
  //   @Get('profile')
  //   getProfile(@Request() req) {
  //     return req.user;
  //   }
  @Post('login')
  public async login(@Body() login: Login) {
    try {
      var respone = await this.authService.validateLogin(login.email, login.password);
      return respone;

    }
    catch (e) {
      return e;
    }
  }
  @Post('register')
  async register(@Body() user: UserEntity) {
    try {
      var newUser = await this.userService.createNewUser(user);
      return newUser;
    }
    catch (e) {
      return e;
    }
  }
}
